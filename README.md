# JobMining
## Введение
Это своего рода журнал поиска работы. 
Изначально это было обычное однопользовательское desktop приложение. Теперь же я хочу создать 
полноценное web приложение.

## Технологии
В данном проекте используется Python 3.8, Django 3.1, Postgres 12, Nginx и Gunicorn.
Виртуальное окружение реализовано с помощью python venv. Для связи с базой данных - 
psycopg2 2.8.

## Запуск
Прежде всего необходимо создать файл JobMining/.env. Вот пример:
```
DEBUG=True
SECRET_KEY=qsjg07^j9fb8i9^7d0e*95%y&0j2*n_(jd9=13ws#c&8vbe8#1
DATABASE_NAME=db1
DATABASE_USER=db1_user
DATABASE_PASSWORD=db1_user_pass
DATABASE_HOST=db1
DATABASE_PORT=5432
``` 
Для файла журнала требуется создать каталог logs.  
Затем нужно выполнить миграции:
```
docker-compose exec jobminingapp python manage.py migrate
```
после чего создать суперпользователя:
```
docker-compose exec jobminingapp python manage.py createsuperuser
```
Или же воспользоваться примером, для чего выполнить:
```
cat db_backups/jm.dump | docker exec -i jobmining_db1_1 psql --user=db1_user --dbname=db1
```