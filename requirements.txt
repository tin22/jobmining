asgiref==3.2.10
backcall==0.2.0
certifi==2020.6.20
cffi==1.14.3
chardet==3.0.4
cryptography==3.1.1
decorator==4.4.2
defusedxml==0.6.0
Django==3.1.1
django-allauth==0.43.0
django-crispy-forms==1.9.2
django-environ==0.4.5
django-markdownx==3.0.1
gunicorn==20.0.4
idna==2.10
ipython==7.18.1
ipython-genutils==0.2.0
jedi==0.17.2
Markdown==3.2.2
oauthlib==3.1.0
parso==0.7.1
pexpect==4.8.0
pickleshare==0.7.5
Pillow==7.2.0
prompt-toolkit==3.0.7
psycopg2==2.8.6
ptyprocess==0.6.0
pycparser==2.20
Pygments==2.7.1
PyJWT==1.7.1
python3-openid==3.2.0
pytz==2020.1
requests==2.24.0
requests-oauthlib==1.3.0
six==1.15.0
sqlparse==0.3.1
traitlets==5.0.4
urllib3==1.25.10
wcwidth==0.2.5
whitenoise==5.2.0
