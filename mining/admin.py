from django.contrib import admin
from markdownx.admin import MarkdownxModelAdmin
from .models import Advert, Activity


class ActivityInline(admin.TabularInline):
    model = Activity

    extra = 0


@admin.register(Advert)
class AdvertAdmin(admin.ModelAdmin):
    list_display = ('pub_date', 'organization', 'post', 'status', )
    ordering = ('-pub_date', )

    inlines = [ActivityInline, ]


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ['timestamp', 'action', ]
    ordering = ['-timestamp', ]
