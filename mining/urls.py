from django.conf.urls import url
from django.urls import path, include
from markdownx import urls as markdownx

from .views import advert_list, advert_detail, ActionListView, AdvertListView

app_name = 'mining'
urlpatterns = [
    path('', AdvertListView.as_view(), name='advert_list'),   # advert_list, name='advert_list'),
    path('<uuid:advert_id>/', advert_detail, name='advert_detail'),
    path('actions/', ActionListView.as_view(), name='all_action_list'),

    url(r'^markdownx/', include(markdownx)),
]
