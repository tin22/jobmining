from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView

from .forms import ActionForm
from .models import Advert, Activity
import logging
import sys

logger = logging.getLogger()
logger.exception(f'{str(sys.executable)}')


class AdvertListView(ListView):
    model = Advert
    template_name = 'mining/advert_list.html'
    context_object_name = 'all_advert_list'
    ordering = ('-pub_date', )

    def get_queryset(self):
        return Advert.objects.filter(status=True)


class ActionListView(ListView):
    model = Activity
    context_object_name = 'all_action_list'
    template_name = 'mining/action_list.html'

    def get_queryset(self):
        return Activity.objects.order_by('-timestamp')[:15]


class AdvertDetailView(DetailView):
    model = Advert


def advert_list(request):
    all_advert_list = Advert.objects.order_by('-pub_date')
    context = {'all_advert_list': all_advert_list}
    return render(request, 'mining/advert_list.html', context)


def advert_detail(request, advert_id):
    advert = get_object_or_404(Advert, pk=advert_id)
    actions = advert.actions.all()
    new_action = None

    if request.method == 'POST':
        # A new action was added
        action_form = ActionForm(data=request.POST)
        if action_form.is_valid():
            # Create action object, but don't save to database yet.
            new_action = action_form.save(commit=False)
            # Assign the current advert to the action
            new_action.advert = advert
            new_action.author = request.user
            # save the action to database
            new_action.save()
    else:
        action_form = ActionForm()
    return render(request, "mining/advert_detail.html",
                  {'advert': advert,
                   'actions': actions,
                   'new_action': new_action,
                   'action_form': action_form})
