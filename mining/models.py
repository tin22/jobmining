import uuid
from django.contrib.auth import get_user_model
from django.db import models
from markdownx.models import MarkdownxField
import logging

logger = logging.getLogger(__name__)


class Activity(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    advert = models.ForeignKey('Advert', models.DO_NOTHING, related_name='actions')
    action = models.TextField('Действие')
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE
    )

    class Meta:
        db_table = 'activity'
        verbose_name_plural = 'actions'

    def __str__(self):
        return f"{self.action}"


class Advert(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False)
    organization = models.CharField(max_length=60)
    post = models.CharField(max_length=60)
    text = MarkdownxField()
    salary = models.CharField(max_length=60, blank=True, null=True)
    person = models.CharField(max_length=60)
    phone = models.CharField(max_length=60)
    pub_date = models.DateField(blank=True, null=True)
    status = models.BooleanField()

    class Meta:
        db_table = 'advert'
        ordering = ('-pub_date',)

    def __str__(self):
        return f"{self.organization}. {self.post}"


    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     """
    #     При создании объявления добавить действие 'Объявление размещено в БД.'
    #     """
    #     Activity.objects.create(advert=self, action='Объявление размещено в БД.',
    #                             author=get_user_model().objects.get(pk=1))
    #     self.status = True
    #     super(Advert, self).save()
    #