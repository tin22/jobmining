from django import forms

from .models import Activity


class ActionForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ('action', )
