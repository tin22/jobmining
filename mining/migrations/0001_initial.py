# Generated by Django 3.1.1 on 2020-10-09 07:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import markdownx.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Advert',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('organization', models.CharField(max_length=60)),
                ('post', models.CharField(max_length=60)),
                ('text', markdownx.models.MarkdownxField()),
                ('salary', models.CharField(blank=True, max_length=60, null=True)),
                ('person', models.CharField(max_length=60)),
                ('phone', models.CharField(max_length=60)),
                ('pub_date', models.DateField(blank=True, null=True)),
                ('status', models.BooleanField()),
            ],
            options={
                'db_table': 'advert',
            },
        ),
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('action', models.TextField(verbose_name='Действие')),
                ('advert', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='actions', to='mining.advert')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'actions',
                'db_table': 'activity',
            },
        ),
    ]
