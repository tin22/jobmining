# Start from an official image
FROM python:3.8

# Arbitrary location choice
RUN mkdir -p /opt/services/JobMiningApp/src
WORKDIR /opt/services/JobMiningApp/src
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Copy our project code
COPY . /opt/services/JobMiningApp/src

# Install our dependencies
RUN pip install -r requirements.txt

# EXPOSE the port 8000
EXPOSE 8000

# Define the default command to run when starting the container
CMD ["gunicorn", "--bind", ":8000", "JobMining.wsgi:application"]

